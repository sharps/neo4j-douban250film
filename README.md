# neo4j-douban250film

**步骤一、**运行 runspider.py 爬取豆瓣电影top250资料，并在根路径下生成 Douban.csv

**步骤二、**运行 Douban_node.py生成Neo4j需要的node结点

**步骤三、**运行 Douban_relationship.py生成Neo4j需要的relationship关系

**启动neo4j**

sharps@sharps-computer:~/neo4j-community-3.5.5/bin$ ./neo4j start

**导入CSV**

拷贝out中生成node和relationship文件到 neo4j程序bin目录下的import

**执行导入命令**

./neo4j-admin import  --mode csv --database film.db --nodes import/actor.csv --nodes import/director.csv --nodes import/film.csv --nodes import/type.csv --relationships import/relationship_actor_film.csv --relationships import/relationship_director_actor.csv --relationships import/relationship_director_film.csv --relationships import/relationship_film_type.csv

**重启neo4j**

sharps@sharps-computer:~/neo4j-community-3.5.5/bin$ ./neo4j restart

参考一：豆瓣电影top250内容爬取 

https://blog.csdn.net/PbGc396Dwxjb77F2je/article/details/79832021

参考二：豆瓣电影知识图谱 Neo4j 构建

https://www.jianshu.com/p/fd5a91d16c34
