# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy

#  output_processor把列表换成str,且设置分隔符
#  input_processor去空格，切分等
from scrapy.loader.processors import Join, MapCompose


def mysplit(info):
    return info.split('/')


class PythoneDoubanAllItem(scrapy.Item):
    # define the fields for your item here like:
    movie_name = scrapy.Field(output_processor=Join())  # 电影名称
    movie_directors = scrapy.Field(input_processor=MapCompose(str.strip), output_processor=Join('/'))  # 导演
    movie_actors = scrapy.Field(input_processor=MapCompose(str.strip), output_processor=Join('/'))  # 演员
    # movie_type = scrapy.Field(output_processor=Join(''))  # 类型
    movie_type = scrapy.Field(input_processor=MapCompose(str.strip), output_processor=Join('/'))  # 类型
    movie_showdate = scrapy.Field(input_processor=MapCompose(str.strip), output_processor=Join(''))  # 上映时间
    movie_runtime = scrapy.Field(input_processor=MapCompose(str.strip), output_processor=Join(''))  # 时长
    movie_showplace = scrapy.Field(input_processor=MapCompose(str.strip), output_processor=Join(''))  # 上映地
    movie_language = scrapy.Field(input_processor=MapCompose(str.strip), output_processor=Join(''))  # 语言
    movie_othername = scrapy.Field(input_processor=MapCompose(mysplit, str.strip), output_processor=Join(''))  # 又名
    movie_score = scrapy.Field(input_processor=MapCompose(str.strip), output_processor=Join(''))  # 评分
    movie_vote = scrapy.Field(input_processor=MapCompose(str.strip), output_processor=Join(''))  # 投票人数




