# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
# csv---文本操作
# txt---文本操作
# json---文本操作


import csv

from pandas._libs import json


class PythoneDoubanAllPipeline(object):  # 名称可以自定义，只需要和setting中对应即可

    def __init__(self):
        # csv---文本操作
        self.csv_file = open('Douban.csv', 'w')
        self.writer = csv.writer(self.csv_file)
        self.rows = ('filmname', 'directors', 'actors', 'type')
        self.writer.writerow(self.rows)
        # csv---文本操作

        # txt---文本操作
        self.txt_file = open('Douban.txt', 'w', encoding='utf-8')
        # txt---文本操作

        # json---文本操作
        self.json_file = open('Douban.json', 'w', encoding='utf-8')
        # self.json_file.write('[')
        # json---文本操作

    def open_spider(self, spider):
        """爬虫开始执行时，调用，可以通过sprint(‘打开’)来测试"""
        # txt---文本操作
        # self.txt_file = open('top250.txt', 'w', encoding='utf-8')
        # txt---文本操作

    def close_spider(self, spider):
        """爬虫关闭执行时，调用，可以通过sprint(‘关闭’)来测试"""
        # txt---文本操作
        self.txt_file.close()  # 关闭文件
        # txt---文本操作

        # csv---文本操作
        # self.writer.close()
        self.csv_file.close()
        # csv---文本操作

        # json---文本操作
        # self.json_file.write(']')
        self.json_file.close()
        # json---文本操作

    def process_item(self, item, spider):
        # txt---文本操作
        """每个item都需要在这里处理"""
        # 处理需要保存的信息
        info = ''
        for i in item.values():
            info = info + ',' + i
        # 写入文件
        self.txt_file.write(info)
        self.txt_file.write("\n")
        # txt---文本操作

        # csv---文本操作
        # 判断字段值不为空再写入文件
        if item['movie_name']:
            self.writer.writerow((item['movie_name'],
                                 item['movie_directors'],
                                 item['movie_actors'],
                                 item['movie_type']))
        # csv---文本操作

        # json---文本操作
        line = json.dumps(dict(item), ensure_ascii=False)
        self.json_file.write(line)
        self.json_file.write(',\n')
        # json---文本操作

        return item  # 不丢弃。 把pipeline交给下一个

